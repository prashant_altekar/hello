package com.example.helloandroid;

import java.io.InputStream;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.graphics.Color;

public class HelloAndroidActivity extends Activity {
    private boolean onePressed = false, twoPressed = false, done = false;
    RelationShipStrings rsStrings;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InputStream is = this.getResources().openRawResource(R.string.RelationShipStrings);
        rsStrings = new RelationShipStrings(is);
        setContentView(R.layout.relationshipview);
        clearMessage();
    }

    public void sendClick1(View view) {
        onePressed = true;
        sendClick (R.id.button1);
    }

    public void sendClick2(View view) {
        twoPressed = true;
        sendClick (R.id.button2);
    }

    private void sendClick(int button)
    {
        Button pressedButton = (Button)findViewById(button);
        pressedButton.setEnabled(false);

        if (onePressed && twoPressed && !done) {
            done = true;
            showMessage();
        }
    }

    private void showMessage()
    {
        TextView textView = (TextView)findViewById(R.id.text_message);
        textView.setText(rsStrings.getRandomMessage());
        textView.setTextColor(Color.WHITE);
    }

    public void resetClicks(View view) {
        onePressed = false;
        ((Button)findViewById(R.id.button1)).setEnabled(true);
        twoPressed = false;
        ((Button)findViewById(R.id.button2)).setEnabled(true);
        done = false;
        clearMessage();
    }

    private void clearMessage()
    {
        TextView textView = (TextView)findViewById(R.id.text_message);
        textView.setText(getResources().getText(R.string.Message));
        textView.setTextColor(Color.GRAY);
    }

}