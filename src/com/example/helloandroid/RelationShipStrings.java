/*
 * CONFIDENTIAL COMPUTER CODE AND INFORMATION
 * COPYRIGHT (C) 2000-2012 VENDAVO, INC. ALL RIGHTS RESERVED.
 * REPRODUCTION BY ANY MEANS EXPRESSLY FORBIDDEN WITHOUT THE WRITTEN
 * PERMISSION OF THE OWNER.
 */
package com.example.helloandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class RelationShipStrings
{
    ArrayList<String> strings = new ArrayList<String>();

    public RelationShipStrings(InputStream is)
    {
        String str;
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        try {
            if (is != null) {
                while ((str = reader.readLine()) != null) {
                    strings.add(str);
                }
            }
            is.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getRandomMessage()
    {
        return strings.get((int)(Math.random() * strings.size()));
    }
}
